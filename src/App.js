import { useEffect, useState } from "react";
import "./App.css";
import Choices from "./components/Choices";
import Round from "./components/Round";
import Score from "./components/Score";

const App = () => {
  const choices = [
    {
      name: "rock",
      beats: "scissors",
      emoji: "✊",
    },
    {
      name: "paper",
      beats: "rock",
      emoji: "✋",
    },
    {
      name: "scissors",
      beats: "paper",
      emoji: "✌️",
    },
  ];
  const [init, setInit] = useState(false);
  const [gameOver, setGameOver] = useState("");
  const [cpuChoice, setCpuChoice] = useState({});
  const [playerChoice, setPlayerChoice] = useState({});
  const [playerWin, setPlayerWin] = useState(false);
  const [cpuWin, setCpuWin] = useState(false);
  const [playerScore, setPlayerScore] = useState(0);
  const [cpuScore, setCpuScore] = useState(0);
  const [result, setResult] = useState("");

  useEffect(() => {
    setPlayerWin(compare(playerChoice, cpuChoice));
    setCpuWin(compare(cpuChoice, playerChoice));
  }, [playerChoice, cpuChoice]);

  useEffect(() => {
    result === "Calculating" && setResult(roundResult());
    if (playerScore >= 10) {
      setTimeout(() => {
        setGameOver("Player Wins!");
        setInit(false);
      }, 1000);
    } else if (cpuScore >= 10) {
      setTimeout(() => {
        setGameOver("Cpu Wins!");
        setInit(false);
      }, 1000);
    }
  }, [result]);

  const handleCpuChoice = () => {
    return choices[Math.floor(Math.random() * 3)];
  };

  const handleInit = () => {
    setInit(!init);
    setCpuChoice({});
    setPlayerChoice({});
    setResult("");
    setGameOver("");
    setPlayerScore(0);
    setCpuScore(0);
  };

  const roundResult = () => {
    const round =
      playerWin && !cpuWin ? "Win" : !playerWin && cpuWin ? "Loss" : "Draw";
    round === "Win" && setPlayerScore(playerScore + 1);
    round === "Loss" && setCpuScore(cpuScore + 1);
    return round;
  };

  const handleChoice = (newPlayerChoice) => {
    setPlayerChoice(newPlayerChoice);
    setCpuChoice(handleCpuChoice());
  };

  const compare = (choice, opponent_choice) => {
    setResult("Calculating");
    const { beats } = choice;
    const { name } = opponent_choice;
    return beats === name;
  };

  return (
    <div className="App">
      <header className="App-header">
        {gameOver && <h1>{gameOver}</h1>}
        {init ? (
          <>
            <Score playerScore={playerScore} cpuScore={cpuScore} />
            {result !== "Calculating" && playerChoice.name && (
              <Round
                result={result}
                playerWin={playerWin}
                cpuWin={cpuWin}
                player={playerChoice}
                cpu={cpuChoice}
              />
            )}

            <section className="choiceSection">
              {choices.map((choice) => {
                return (
                  <Choices handleChoice={handleChoice} choice={choice}>
                    {choice.emoji}
                  </Choices>
                );
              })}
            </section>
          </>
        ) : (
          <button onClick={() => handleInit()}>New Game</button>
        )}
      </header>
    </div>
  );
};

export default App;
