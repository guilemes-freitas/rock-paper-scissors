import "./styles.css";
const Round = (props) => {
  return (
    <section className="roundSection">
      <h2>{props.result}</h2>
      <div className="roundContainer">
        <div>{props.player.emoji}</div>
        <p>X</p>
        <div>{props.cpu.emoji}</div>
      </div>
    </section>
  );
};

export default Round;
