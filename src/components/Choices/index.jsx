import "./styles.css";

const Choices = (props) => {
  return (
    <button className="choice" onClick={() => props.handleChoice(props.choice)}>
      {props.children}
    </button>
  );
};

export default Choices;
