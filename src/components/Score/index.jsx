import "./styles.css";

const Score = (props) => {
  return (
    <section className="scoreContainer">
      <h2>Score</h2>
      <div className="score">
        <div className="scoreNumber">
          <h3>{props.playerScore}</h3>
          <span>Player</span>
        </div>
        <div className="scoreNumber">
          <h3>{props.cpuScore}</h3>
          <span>CPU</span>
        </div>
      </div>
    </section>
  );
};

export default Score;
